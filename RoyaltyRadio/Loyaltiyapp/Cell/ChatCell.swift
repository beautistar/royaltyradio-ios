//
//  MenuCell.swift
//  PortTrucker
//
//  Created by Admin on 2019/8/29.
//  Copyright © 2019 LiMing. All rights reserved.
//

import UIKit
import Kingfisher

class ChatCell: UITableViewCell {
    
    @IBOutlet weak var imvPhoto: UIImageView!
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblContent: UILabel!
    @IBOutlet weak var youContent: UIView!
    
    @IBOutlet weak var myProfile: UIImageView!
    @IBOutlet weak var lblmyName: UILabel!
    @IBOutlet weak var lblmyTime: UILabel!
    @IBOutlet weak var lblmyContent: UILabel!
    @IBOutlet weak var meContent: UIView!
    
    var entity : ChatModel!{
        didSet {
            let dateString = getStrDate(entity.timestamp)
            if !entity.me {
                
                let imageURL = entity.photoUrl
                let url = URL(string:imageURL)
                //imvPhoto.image = UIImage(named: entity.imvPhoto)
                imvPhoto.kf.setImage(with: url,placeholder: UIImage(named: "avatar-1"))
                lblTitle.text = entity.username
                
                lblTime.text = dateString
                lblContent.text = entity.msgContent
                    
                myProfile.isHidden = true
                meContent.isHidden = true
                imvPhoto.isHidden = false
                youContent.isHidden = false

               
            }
                
              else if entity.me  {
                           
               let imageURL = entity.photoUrl
               let url = URL(string:imageURL)
               //imvPhoto.image = UIImage(named: entity.imvPhoto)
               myProfile.kf.setImage(with: url,placeholder: UIImage(named: "avatar-1"))
               lblmyName.text = entity.username
               lblmyTime.text = dateString
               lblmyContent.text = entity.msgContent
                   
               imvPhoto.isHidden = true
               youContent.isHidden = true
                myProfile.isHidden = false
                meContent.isHidden = false
                
               }
        }
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
}
