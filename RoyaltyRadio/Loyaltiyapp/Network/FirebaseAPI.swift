
import Foundation
import Firebase
import SwiftyUserDefaults

class FirebaseAPI {
    
    static let ref = Database.database().reference()
    
    static let CONV_REFERENCE = "message"

    
    // MARK: - Set/remove Add/change observer
    static func setRoyaltyListener(_ roomId: String, handler:@escaping (_ msg: ChatModel)->()) -> UInt {
        
        return ref.child(CONV_REFERENCE).child(roomId).queryLimited(toFirst: 100).observe(.childAdded) { (snapshot, error) in
            
            let childref = snapshot.value as? NSDictionary
            if let childRef = childref {
                let msg = parseMsg(childRef)
                handler(msg)
            } else {
            }
        }
    }
    
    
    static func parseMsg(_ snapshot: NSDictionary) -> ChatModel {
        
        let msg = ChatModel( me : false,photoUrl: "", username: "", timestamp: "", msgContent: "", senderId: "")
        
        if !guestMode{
            
            msg.msgContent = snapshot["message"] as! String
            msg.username   = snapshot["name"] as! String
            msg.photoUrl   = snapshot["photo"] as! String
            msg.senderId   = snapshot["sender_id"] as! String
            msg.timestamp  = snapshot["time"] as! String
            if msg.senderId == Defaults[.id]! as String {
                msg.me = true
            }
        }
        else {
            
            
            msg.msgContent = snapshot["message"] as! String
            msg.username   = snapshot["name"] as! String
            msg.photoUrl   = snapshot["photo"] as! String
            msg.senderId   = snapshot["sender_id"] as! String
            msg.timestamp  = snapshot["time"] as! String
            
            if msg.senderId == geustSenderID {
               msg.me = true
            }
            
        }
        
        return msg
    }
    
    static func removeRoyaltyObserver(_ roomId: String, _ handle : UInt) {
        ref.child(CONV_REFERENCE).child(roomId).removeObserver(withHandle: handle)
    }
    
    
    
    // MARK: - send Chat
    static func sendMessage(_ chat:[String:String], _ roomId: String, completion: @escaping (_ status: Bool, _ message: String) -> ()) {
        
        ref.child(CONV_REFERENCE).child(roomId).childByAutoId().setValue(chat) { (error, dataRef) in
            if let error = error {
                completion(false, error.localizedDescription)
            } else {
                completion(true, "Sent a message successfully.")
            }
        }
    }
    
}
