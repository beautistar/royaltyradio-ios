//
//  BaseVC1.swift
//  Loyaltiyapp
//
//  Created by PSJ on 10/21/19.
//  Copyright © 2019 PSJ. All rights reserved.
//

import UIKit
import Toast_Swift
import SVProgressHUD
import SwiftyUserDefaults
import SwiftyJSON

class BaseVC1: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func showMessage1(_ message : String) {
        self.view.makeToast(message)
    }
    
    func gotoNavPresent1(_ storyname : String) {
              
              let toVC = self.storyboard?.instantiateViewController(withIdentifier: storyname)
              toVC?.modalPresentationStyle = .fullScreen
             self.navigationController?.isNavigationBarHidden = false
              self.navigationController?.pushViewController(toVC!, animated: true)
             
      }
       
        func navBarHidden() {
           self.navigationController?.isNavigationBarHidden = true
       }
       func navBarTransparent1() {
              self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
              self.navigationController?.navigationBar.shadowImage = UIImage()
              self.navigationController?.navigationBar.isTranslucent = true
              self.navigationController?.view.backgroundColor = .clear
        }
    
    func navBarwithColor() {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
         self.navigationController?.navigationBar.shadowImage = UIImage()
         self.navigationController?.navigationBar.isTranslucent = true
         self.navigationController?.view.backgroundColor = .blue
   }
    
       func showHUD1() {
           SVProgressHUD.show();
       }

       func showHUDWithTitle1(title: String) {
           SVProgressHUD.show(withStatus: title)
       }

       func hideHUD1() {
           SVProgressHUD.dismiss()
       }
    
    func logout() {
        if radio != nil
        {
            radio.pause()
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let nextVC = storyBoard.instantiateViewController(withIdentifier: "LoginNav") as! LoginNav
            nextVC.modalPresentationStyle = .fullScreen
                self.present(nextVC, animated: true, completion: nil)
            
        }
        else{
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let nextVC = storyBoard.instantiateViewController(withIdentifier: "LoginNav") as! LoginNav
        nextVC.modalPresentationStyle = .fullScreen
            self.present(nextVC, animated: true, completion: nil)
            
        }
    }
    
}
extension UITextField {
    
    enum PaddingSide1 {
        case left(CGFloat)
        case right(CGFloat)
        case both(CGFloat)
    }
    
    func addPadding1(_ padding: PaddingSide1) {
        
        self.leftViewMode = .always
        self.layer.masksToBounds = true
        
        switch padding {
            
        case .left(let spacing):
            let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: spacing, height: self.frame.height))
            self.leftView = paddingView
            self.rightViewMode = .always
            
        case .right(let spacing):
            let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: spacing, height: self.frame.height))
            self.rightView = paddingView
            self.rightViewMode = .always
            
        case .both(let spacing):
            let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: spacing, height: self.frame.height))
            self.leftView = paddingView
            self.leftViewMode = .always
            self.rightView = paddingView
            self.rightViewMode = .always
        }
    }
}

extension UIView {
    func dropShadowleft1(scale: Bool = true) {
        layer.masksToBounds = false
        layer.shadowColor = UIColor.white.cgColor
        layer.shadowOpacity = 0.8
        layer.shadowOffset = CGSize(width: -1.0, height: 1.0)
        layer.shadowRadius = 3.0
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
    
    func ShadowCenter(scale: Bool = true) {
           layer.masksToBounds = false
           layer.shadowColor = UIColor.white.cgColor
           layer.shadowOpacity = 0.8
           layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
           layer.shadowRadius = 3.0
           layer.shouldRasterize = true
           layer.rasterizationScale = scale ? UIScreen.main.scale : 1
       }
    
    func dropShadowbottom1(scale: Bool = true) {
        layer.masksToBounds = false
        layer.shadowColor = UIColor.white.cgColor
        layer.shadowOpacity = 0.7
        layer.shadowOffset = CGSize(width: 0.0, height:4.0)
        layer.shadowRadius = 2.0
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
    
}
