//
//  SocialMedia.swift
//  Loyaltiyapp
//
//  Created by PSJ on 10/17/19.
//  Copyright © 2019 PSJ. All rights reserved.
//

import UIKit

class SocialMedia: BaseVC1 {
    
    
    @IBOutlet weak var contentView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        
        contentView.frame.size.width = UIScreen.main.bounds.width / 3 * 2.5
      
        navBarTransparent1()
        
    }
    @IBAction func fbBtnClicked(_ sender: Any) {
        gotoNavPresent1("Webview")
        webviewIndex = 0
    }
    @IBAction func twBtnClicked(_ sender: Any) {
        gotoNavPresent1("Webview")
        webviewIndex = 1
    }
    @IBAction func insBtnClicked(_ sender: Any) {
        gotoNavPresent1("Webview")
        webviewIndex = 2
    }
    @IBAction func homeBtnClicked(_ sender: Any) {
        gotoNavPresent1("Webview")
        webviewIndex = 3
    }
    @IBAction func aboutBtnClicked(_ sender: Any) {
        gotoNavPresent1("Webview")
        webviewIndex = 4
    }
    
    @IBAction func handleBackButtonClick(_ sender: Any) {// back action
         let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
               let nextVC = storyBoard.instantiateViewController(withIdentifier: "MainNav") as! MainNav
               nextVC.modalPresentationStyle = .fullScreen
               self.present(nextVC, animated: true, completion: nil)
               calendarScreen = true
                if status{
                   radio.updatePlay(true)
               }
               else{
                   print("pause")
               }
        
    }
}
