//
//  MenuVC.swift
//  PortTrucker
//
//  Created by LiMing on 8/13/19.
//  Copyright © 2019 LiMing. All rights reserved.
//

import Foundation
import UIKit
import SwiftyUserDefaults
import Kingfisher

class MenuVC : BaseVC1, UITableViewDelegate, UITableViewDataSource{
   
    
    @IBOutlet weak var spaceView: UIView!
    @IBOutlet weak var tbvMenu: UITableView!
    @IBOutlet weak var userProfile: UIImageView!
    @IBOutlet weak var usrname: UILabel!
    
    var dataSource =  [MenuModel]()
    
     var window: UIWindow?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadData()
    
        profileInit()
        
    }
    
    func profileInit() {
        let imageURL = Defaults[.profile]
        let url = URL(string:imageURL!)
        userProfile.kf.setImage(with: url, placeholder: UIImage(named: "avatar-1"))
        
        usrname.text = Defaults[.username]
        
    }
    
    func loadData() {
        let menuIcons = ["radio","browse","event","music","signout"]
        let menuText = ["Radio & Chat","Live Video & Chat","Events & Calendar","Social Media","Sign out"]
        
        for i in 0 ..< menuIcons.count {
            let one = MenuModel.init(icon: menuIcons[i], title: menuText[i])
            dataSource.append(one)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tbvMenu.dequeueReusableCell(withIdentifier: "MenuCell", for:indexPath) as! MenuCell
        cell.entity = dataSource[indexPath.row]
        let row = indexPath.row
        
        if row == 3{
            cell.divider.isHidden = false
        }

        return cell
    }
    // didselect event
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       tableView.cellForRow(at: indexPath)?.backgroundColor = UIColor.init(named: "menuClick")
        let row = indexPath.row // new rowvalue
        
                if row == 2 {
                    
                    let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let nextVC = storyBoard.instantiateViewController(withIdentifier: "CalendarNav") as! CalendarNav
                    nextVC.modalPresentationStyle = .fullScreen
                    self.present(nextVC, animated: true, completion: nil)
                    
                }
                
                if row == 3 {

                    let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let nextVC = storyBoard.instantiateViewController(withIdentifier: "SocialNav") as! SocialNav
                    nextVC.modalPresentationStyle = .fullScreen
                    self.present(nextVC, animated: true, completion: nil)
                 
                }
                
                if row == 4 {
                    logout()
                }
                
                if  row != 2 && row != 3 && row != 4 {
                    gotoNavPresent1(storyName[row])
                   
                }
                else{
                    print("printed")
                    }
               // closeMenu()
                    print(row)
                    lastIndexValue = 0
        }
    // didselelct color
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        tableView.cellForRow(at: indexPath)?.backgroundColor = UIColor.init(named: "menuClick")
    }
}
