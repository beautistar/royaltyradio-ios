//
//  Splash.swift
//  Loyalty
//
//  Created by PSJ on 10/11/19.
//  Copyright © 2019 PSJ. All rights reserved.
//

import UIKit
import SwiftyUserDefaults
import SwiftyJSON

class Splash: BaseVC1 {

    override func viewDidLoad() {
        super.viewDidLoad()
        gotoHome()

    }
    
    func gotoHome() {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
            
            if Defaults[.email] == nil {
               let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
               let nextVC = storyBoard.instantiateViewController(withIdentifier: "LoginNav") as! LoginNav
               nextVC.modalPresentationStyle = .fullScreen
                self.present(nextVC, animated: true, completion: nil)
            }
            else {
                
                self.showHUD1()
                ApiManager.login(useremail: "\(Defaults[.email] ?? "" as Any)", password: "\(Defaults[.pwd] ?? ""  as Any)") { (isSuccess, data) in
                              self.hideHUD1()
                              if isSuccess{
                                  let dict = JSON(data as Any)
                                  print(dict)
                                  GlobalUser = UserModel(dict)
                                
                                print("++++++++++++++++++++++++++++++")
                                  print(GlobalUser.photo)
                                  
                                  //Defaults[.email] = useremail
                                 // Defaults[.profile] = GlobalUser.photo
                                  //Defaults[.username] = GlobalUser.username
                                  //Defaults[.id] = GlobalUser.user_id
                                
                                  is_admin = GlobalUser.is_admin
                                  block = GlobalUser.block
                                  report = GlobalUser.report
                                print("----------------------------------")
                               print("\(Defaults[.id]! as Any)")
                              }
                              else{
                                  if data == nil{
                                      self.showMessage1("Connection Fail")
                                  }
                                  else{
                                      let result_message = data as! String
                                      if(result_message == "1"){
                                          self.showMessage1("Non Exist Email")
                                      }
                                      else{
                                          self.showMessage1("Password Incorrect")
                                      }
                                  }
                                  
                              }
                              
                          }
                
                let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let nextVC = storyBoard.instantiateViewController(withIdentifier: "MainNav") as! MainNav
                nextVC.modalPresentationStyle = .fullScreen
                 self.present(nextVC, animated: true, completion: nil)
            }
           })
       }
    
}
