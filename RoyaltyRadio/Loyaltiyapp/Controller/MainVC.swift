//
//  MainVC.swift
//  PortTrucker
//
//  Created by LiMing on 8/13/19.
//  Copyright © 2019 LiMing. All rights reserved.
//

import Foundation
import UIKit
import MediaPlayer
import SwiftyUserDefaults
import AZDialogView
import Firebase



class MainVC : BaseVC1 , UITableViewDelegate, UITableViewDataSource , RadioDelegate,
    
AVAudioPlayerDelegate  {
    
    @IBOutlet weak var shadowEffect: UIView?
    var audiodataSource =  [ChatModel]()
    @IBOutlet weak var tbvChat: UITableView?
    
    
    @IBOutlet weak var imvPlay: UIImageView?
    @IBOutlet weak var imvPause: UIImageView?
    @IBOutlet weak var songName: UILabel?
    @IBOutlet weak var songArtist: UILabel?
    @IBOutlet weak var topmenu: UIImageView!
    @IBOutlet weak var guestBack: UIButton?
    @IBOutlet weak var imvGuest: UIImageView?
    @IBOutlet weak var ui_txtMsg: UITextField!
    let chatRoomId = "royaltyradio_audio"
    var newChatHandle: UInt?
    var timer:Timer = Timer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        navBarTransparent1()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        if !guestMode{
            
            guestBack?.isHidden = true
            imvGuest?.isHidden = true
        }
        
        else {
            topmenu.isHidden = true
        }
        
        shadowEffect?.ShadowCenter()
        
        tbvChat?.estimatedRowHeight = 110
        radio = Radio.init(userAgent: "RadioKaktus")
        if calendarScreen {
            //radioInitChange()
            if status{
                radio.pause()
                radioInitChange()
                imvPlay?.isHidden = true
                imvPause?.isHidden = false
                playSound()
            
            }
            else{
                radioInitChange()
            }
            calendarScreen = false
        }
        else{
            radio.pause()
            radioInitChange()
            
        }
        fireBaseNewChatListner()
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        //timer.invalidate()
       // FirebaseAPI.removeRoyaltyObserver(chatRoomId, newChatHandle!)
    }
  

    @IBAction func guestBackBtnClicked(_ sender: Any) {
       logout()
       guestMode = false
    }
    
    func fireBaseNewChatListner()  {
        newChatHandle = FirebaseAPI.setRoyaltyListener( chatRoomId){ (msgModel) in
            self.audiodataSource.append(msgModel)
            self.tbvChat?.reloadData()
            self.scrollToBottom()
        }
    }
    
    func scrollToBottom(animated: Bool = true, delay: Double = 0.0) {
        let numberOfRows = tbvChat!.numberOfRows(inSection: tbvChat!.numberOfSections - 1) - 1
        guard numberOfRows > 0 else { return }

        DispatchQueue.main.asyncAfter(deadline: .now() + delay) { [unowned self] in

            let indexPath = IndexPath(
                row: numberOfRows,
                section: self.tbvChat!.numberOfSections - 1)
            self.tbvChat!.scrollToRow(at: indexPath, at: .bottom, animated: animated)
        }
    }
    
    
    @IBAction func btnSendAction(_ sender: Any) {
        
        if checkValid() {
            doSend()
           
            
        }
    }
    
    func checkValid() -> Bool {
        
        self.view.endEditing(true)
        
        if ui_txtMsg.text!.isEmpty {
            
            
            return false
        }
        
        return true
    }
    
    func doSend() {
        
        let timeNow = Int(NSDate().timeIntervalSince1970) * 1000
        
        var chatObject = [String: String]()
        
        
        if !guestMode
               
            {
                
                print(block!)
                print(report!)
                
                if !block!.isEmpty {
                    
                    showMessage1("  You have been blocked! \n Please Contact Admin!  ")
                    self.ui_txtMsg.text! = ""
                    return
                }
                
                if !report!.isEmpty{
                    showMessage1("  You have been reported! \n  Please Contact Admin!  ")
                    self.ui_txtMsg.text! = ""
                    return
                }
                    
                else {
                    
                chatObject["message"]   = ui_txtMsg.text
                chatObject["name"]      = Defaults[.username]
                chatObject["photo"]     = Defaults[.profile]
                chatObject["sender_id"] = Defaults[.id]
                    chatObject["time"]      = "\(timeNow)" as String
                    
                }
                
            }
        else if guestMode{
                showMessage1("You can't send message.Please log in!")
        }
        
        self.ui_txtMsg.text! = ""
               
        FirebaseAPI.sendMessage(chatObject, chatRoomId) { (status, message) in
            
            if status {
                print(message)
            } else {
                print(message)
            }
            
        }
    }
    
    @IBAction func onclickMenu(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func handleBackButtonClick(_ sender: Any) {// back action
        let _ = navigationController?.popViewController(animated: true)
    }
    
    ///  - Description: radio initialize
     func radioInitChange()  {
        
        if status && radio.getstartState() { // radio play state
            imvPlay?.isHidden = true
            imvPause?.isHidden = false
        }
        else{ // radio stop state
            imvPlay?.isHidden = false
            imvPause?.isHidden = true
        }
    }
    
    ///  - Description: radio play
     func playSound() {
        if !status {
            return
        }
        radio.pause()
        radio.connect(channelURL, with: self, withGain: (1.0))
    }
    
    func metaTitleUpdated(_ title: String!) {
        
        //let tempTitle = String(title.replacingOccurrences(of: ";", with: ""))
        let realTitle = String(title.split(separator: "'")[1])
		let songname = String(realTitle.split(separator: "'")[0])
        //let lsongName = String(realTitle.split(separator: "-")[1])
        //let lsongArtist = String(realTitle.split(separator: "-")[0])
        songName?.isHidden = false
        songArtist?.isHidden = false
		songArtist?.text = String(songname.split(separator: ":")[0])
		songName?.text = String(songname.split(separator: ":")[1])
    }
    
    @IBAction func playBtnclicked(_ sender: Any) {
        if status && radio.getstartState(){ // radio play state
            status = false
            radio.pause()
            radioInitChange()
            imvPlay?.isHidden = false
            imvPause?.isHidden = true
            
        } else { // radio stop state
            status = true
            playSound()
            radioInitChange()
            imvPlay?.isHidden = true
            imvPause?.isHidden = false
        }
    }
    func gotoBlockApi(user_id : String) {
        
        ApiManager.block( user_id : user_id ){ (isSuccess, data) in
            
            if isSuccess{
             print("success")
            }
            else{
               print("error")
                
            }
            
        }

    }
    func gotoReportApi(user_id : String) {
        
        ApiManager.report( user_id : user_id ){ (isSuccess, data) in
            
            if isSuccess{
                
             print("success")
            }
            else{
               print("error")
                
            }
            
        }

    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return audiodataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tbvChat?.dequeueReusableCell(withIdentifier: "ChatCell", for:indexPath) as! ChatCell
        cell.entity = audiodataSource[indexPath.row]
        
        let backgroundView = UIView()
        backgroundView.backgroundColor = UIColor.clear
        cell.selectedBackgroundView = backgroundView
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if !audiodataSource[indexPath.row].me && !guestMode && is_admin == "1" {
        let dialog = AZDialogViewController(title: "Do you want to report?", message: "")
        dialog.addAction(AZDialogAction(title: "Block User") { (dialog) -> (Void) in
            //add your actions here.
            
            self.gotoBlockApi(user_id: self.audiodataSource[indexPath.row].senderId)
            
            dialog.dismiss()
            
        })
        dialog.addAction(AZDialogAction(title: "Report User") { (dialog) -> (Void) in
            //add your actions here.
            
            self.gotoReportApi(user_id: self.audiodataSource[indexPath.row].senderId)
            dialog.dismiss()

        })
        
        dialog.addAction(AZDialogAction(title: "Delete Message") { (dialog) -> (Void) in
            //add your actions here.
            dialog.dismiss()
            
            let timestamp = self.audiodataSource[indexPath.row].timestamp

            let ref = Database.database().reference()

            ref.child("message").child(self.chatRoomId).queryOrdered(byChild: "time").queryEqual(toValue: timestamp).observe(.childAdded, with: { (snapshot) in

                   snapshot.ref.removeValue(completionBlock: { (error, reference) in
                       if error != nil {
                           print("There has been an error:\(error)")
                       }
                   })

               })



           self.audiodataSource.remove(at: indexPath.row)
            self.tbvChat!.deleteRows(at: [indexPath], with: .left)
            

        })
        
        dialog.imageHandler = { (imageView) in
            imageView.image = UIImage(named: "warning1")
            imageView.contentMode = .scaleAspectFill
            return true //must return true, otherwise image won't show.
        }
        dialog.titleColor = .purple

        //set the message color
        dialog.messageColor = .purple

        //set the dialog background color
        dialog.alertBackgroundColor = .white

        //set the gesture dismiss direction
        dialog.dismissDirection = .top

        //allow dismiss by touching the background
        dialog.dismissWithOutsideTouch = true

        //show seperator under the title
        dialog.showSeparator = false

        //set the seperator color
        dialog.separatorColor = UIColor.purple

        dialog.show(in: self)
        
            print("\(indexPath.row)")
            
        }
        else{
            print("me")
        }
    }
   
}







 
