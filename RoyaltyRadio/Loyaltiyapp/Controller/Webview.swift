//
//  Webview.swift
//  Loyaltiyapp
//
//  Created by PSJ on 10/23/19.
//  Copyright © 2019 PSJ. All rights reserved.
//

import UIKit
import WebKit

class Webview: BaseVC1 {

    @IBOutlet weak var webView: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
            
        let url = URL(string: socialSites[webviewIndex])!
        webView.load(URLRequest(url: url))
    }
    @IBAction func gotoSocial(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        
    }
    
}
