//
//  ViewController.swift
//  Loyaltiyapp
//
//  Created by PSJ on 10/17/19.
//  Copyright © 2019 PSJ. All rights reserved.
//

import Foundation
import UIKit
import MediaPlayer
import SwiftyUserDefaults
import AZDialogView
import Firebase
import WebKit


class LiveVC : BaseVC1 , UITableViewDelegate, UITableViewDataSource {
    
   

    @IBOutlet weak var topbar: UIView!
    var livedataSource =  [ChatModel]()
    @IBOutlet weak var tbvChat: UITableView!
    let chatRoomId = "royaltyradio_video"
    var newChatHandle: UInt?
    var timer:Timer = Timer()
    
    @IBOutlet weak var webView: WKWebView!
    
    
    @IBOutlet weak var ui_txtMsg: UITextField!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        navBarTransparent1()
        
        if radio != nil{
            
            radio.pause()
            
        }
        fireBaseNewChatListner()
        
                
        let url = URL(string: "https://iframe.dacast.com/b/140114/c/510510")!
                       
        webView.load(URLRequest(url: url))
       
        //***************************************************************//
                        // auto cell height control //
        //***************************************************************//
        tbvChat.estimatedRowHeight = 110
   
        webView.allowsLinkPreview = false

    }
    
    override func viewWillAppear(_ animated: Bool) {
          super.viewWillAppear(animated)
         
    }
       
    
    override func viewWillDisappear(_ animated: Bool) {
        //timer.invalidate()
        FirebaseAPI.removeRoyaltyObserver(chatRoomId, newChatHandle!)
    }
   
    @IBAction func gotoMain(_ sender: Any) {
        print("clieked")
        FirebaseAPI.removeRoyaltyObserver(chatRoomId, newChatHandle!)
        self.navigationController?.popViewController(animated: true)
         calendarScreen = true
        if status{
            radio.updatePlay(true)
        }
        else{
            print("pause")
        }
    }
    
    func gotoBlockApi(user_id : String) {
        
        ApiManager.block(user_id: user_id ){ (isSuccess, data) in
            
            if isSuccess{
                
             print("success")
            }
            else{
               print("error")
                
            }
            
        }

    }
    func gotoReportApi(user_id : String) {
        
        ApiManager.report(user_id: user_id ){ (isSuccess, data) in
            
            if isSuccess{
                
             print("success")
            }
            else{
               print("error")
                
            }
            
        }

    }

    
    func fireBaseNewChatListner()  {
        newChatHandle = FirebaseAPI.setRoyaltyListener( chatRoomId){ (msgModel) in
            self.livedataSource.append(msgModel)
            
            self.tbvChat.reloadData()
            self.scrollToBottom()
        }
    }
    
   func scrollToBottom(animated: Bool = true, delay: Double = 0.0) {
       let numberOfRows = tbvChat.numberOfRows(inSection: tbvChat.numberOfSections - 1) - 1
       guard numberOfRows > 0 else { return }

       DispatchQueue.main.asyncAfter(deadline: .now() + delay) { [unowned self] in

           let indexPath = IndexPath(
               row: numberOfRows,
               section: self.tbvChat.numberOfSections - 1)
           self.tbvChat.scrollToRow(at: indexPath, at: .bottom, animated: animated)
       }
   }
   
    
    @IBAction func handleBackButtonClick(_ sender: Any) {// back action
           let _ = navigationController?.popViewController(animated: true)
       }
    
    @IBAction func onclickMenu(_ sender: Any) {
       let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let nextVC = storyBoard.instantiateViewController(withIdentifier: "MainNav") as! MainNav
        nextVC.modalPresentationStyle = .fullScreen
        self.present(nextVC, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return livedataSource.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tbvChat.dequeueReusableCell(withIdentifier: "ChatCell", for:indexPath) as! ChatCell
        cell.entity = livedataSource[indexPath.row]
        
        let backgroundView = UIView()
        backgroundView.backgroundColor = UIColor.clear
        cell.selectedBackgroundView = backgroundView
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
    }
    
   func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if !livedataSource[indexPath.row].me && !guestMode {
        let dialog = AZDialogViewController(title: "Do you want to report?", message: "")
        dialog.addAction(AZDialogAction(title: "Block User") { (dialog) -> (Void) in
            //add your actions here.
            
            self.gotoBlockApi(user_id: self.livedataSource[indexPath.row].senderId)
            
            dialog.dismiss()
            
        })
        dialog.addAction(AZDialogAction(title: "Report User") { (dialog) -> (Void) in
            //add your actions here.
            
            self.gotoReportApi(user_id: self.livedataSource[indexPath.row].senderId)
            dialog.dismiss()

        })
        
        dialog.addAction(AZDialogAction(title: "Delete Message") { (dialog) -> (Void) in
            //add your actions here.
            dialog.dismiss()
            
            let timestamp = self.livedataSource[indexPath.row].timestamp

            let ref = Database.database().reference()

            ref.child("message").child(self.chatRoomId).queryOrdered(byChild: "time").queryEqual(toValue: timestamp).observe(.childAdded, with: { (snapshot) in

                   snapshot.ref.removeValue(completionBlock: { (error, reference) in
                       if error != nil {
                           print("There has been an error:\(error)")
                       }
                   })

               })



           self.livedataSource.remove(at: indexPath.row)
            self.tbvChat!.deleteRows(at: [indexPath], with: .left)
            

        })
        
        dialog.imageHandler = { (imageView) in
            imageView.image = UIImage(named: "warning1")
            imageView.contentMode = .scaleAspectFill
            return true //must return true, otherwise image won't show.
        }
        dialog.titleColor = .purple

        //set the message color
        dialog.messageColor = .purple

        //set the dialog background color
        dialog.alertBackgroundColor = .white

        //set the gesture dismiss direction
        dialog.dismissDirection = .top

        //allow dismiss by touching the background
        dialog.dismissWithOutsideTouch = true

        //show seperator under the title
        dialog.showSeparator = false

        //set the seperator color
        dialog.separatorColor = UIColor.purple

        dialog.show(in: self)
        
            print("\(indexPath.row)")
            
        }
        else{
            print("me")
        }
    }
    
    @IBAction func btnSendAction(_ sender: Any) {
        
        if checkValid() {
            doSend()
            
            //self.tbvChat.tablescroll(to: .bottom, animated: true)
        }
    }
    
    func checkValid() -> Bool {
        
        self.view.endEditing(true)
        
        if ui_txtMsg.text!.isEmpty {
            
            
            return false
        }
        
        return true
    }
    
    func doSend() {
        
        let timeNow = Int(NSDate().timeIntervalSince1970) * 1000
        
        var chatObject = [String: String]()
        
        
        if !guestMode
               
            {
                
                print(block!)
                print(report!)
                
                if !block!.isEmpty {
                    
                    showMessage1("  You have been blocked! \n Please Contact Admin!  ")
                    self.ui_txtMsg.text! = ""
                    return
                }
                
                if !report!.isEmpty{
                    showMessage1("  You have been reported! \n  Please Contact Admin!  ")
                    self.ui_txtMsg.text! = ""
                    return
                }
                    
                else {
                    
                chatObject["message"]   = ui_txtMsg.text
                chatObject["name"]      = Defaults[.username]
                chatObject["photo"]     = Defaults[.profile]
                chatObject["sender_id"] = Defaults[.id]
                    chatObject["time"]      = "\(timeNow)" as String
                    
                }
                
            }
        else if guestMode{
                showMessage1("You can't send message.Please log in!")
        }
        
        self.ui_txtMsg.text! = ""
               
        FirebaseAPI.sendMessage(chatObject, chatRoomId) { (status, message) in
            
            if status {
                print(message)
            } else {
                print(message)
            }
            
        }
    }
    
}

extension LiveVC: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if(textField == ui_txtMsg) {
            if (checkValid()) {
                self.doSend()
            }
           
        }
        return true
    }
}
