//
//  MenuModel.swift
//  PortTrucker
//
//  Created by Admin on 2019/8/29.
//  Copyright © 2019 LiMing. All rights reserved.
//

import Foundation

class MenuModel {
    var icon : String
    var title : String
    
    init(icon : String, title : String) {
        self.icon = icon
        self.title = title
    }
}
