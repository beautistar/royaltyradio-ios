//
//  CapturePicModel.swift
//  PortTrucker
//
//  Created by PSJ on 9/17/19.
//  Copyright © 2019 LiMing. All rights reserved.
//

import Foundation
import Foundation
import UIKit

class CapturePicModel {
    
    var imgFile : UIImage?
    var imgUrl : String?
    
    init(_ imgFile : UIImage) {
        self.imgFile = imgFile
    }
    
    init(imgUrl : String) {
        self.imgUrl = imgUrl
    }
}
