//
//  UserModel.swift
//  PortTrucker
//
//  Created by PSJ on 9/14/19.
//  Copyright © 2019 LiMing. All rights reserved.
//

import Foundation
import SwiftyJSON
import Kingfisher
import UIKit

class UserModel {
    var user_id:String = ""
    var email:String = ""
    var username:String = ""
    var photo:String = ""
    var is_admin:String = ""
    var report:String = ""
    var block:String = ""
    
    init (_ Userdata : JSON)
    {
        self.user_id = Userdata [PARAMS.USERID].stringValue
        self.email=Userdata[PARAMS.EMAIL].stringValue
        self.username = Userdata[PARAMS.USERNAME].stringValue
        self.photo = Userdata[PARAMS.PHOTO].stringValue
        self.is_admin = Userdata["is_admin"].stringValue
        self.block = Userdata["block"].stringValue
        self.report = Userdata["report"].stringValue
    }
}
