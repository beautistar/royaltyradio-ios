//
//  ChatModel.swift
//  Loyaltiyapp
//
//  Created by PSJ on 10/17/19.
//  Copyright © 2019 PSJ. All rights reserved.
//

import Foundation
import SwiftyUserDefaults

class ChatModel {
    
    var me : Bool
    var photoUrl : String
    var username : String
    var timestamp : String
    var msgContent : String
    var senderId: String
   

    init(me : Bool, photoUrl: String, username : String, timestamp : String, msgContent : String, senderId: String) {
        
        self.photoUrl = photoUrl
        self.username = username
        self.timestamp = timestamp
        self.msgContent = msgContent
        self.senderId = senderId
        
        self.me = me
    }
}
